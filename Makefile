###############################################################################
#                           RADAR RELATED VARIABLES                           #
###############################################################################
DATE_PREFIX = $(shell date +'%Y_%m_%d-%Hh')
DB_BKP_FILE := bkps/${DATE_PREFIX}_dump.json
MEDIA_BKP := bkps/${DATE_PREFIX}_media.tar

##############
# IMAGE NAME #
##############
RADAR_IMAGE_NAME := registry.gitlab.com/radar-parlamentar/radar/production
RADAR_TEST_IMAGE_NAME := registry.gitlab.com/radar-parlamentar/radar/test

ifndef TAG
	TAG := $(shell git log -n1 --pretty="format:%h")
endif

RADAR_IMAGE_TAGGED = ${RADAR_IMAGE_NAME}:${TAG}
RADAR_IMAGE_LATEST = ${RADAR_IMAGE_NAME}:latest
RADAR_IMAGE_TEST := ${RADAR_TEST_IMAGE_NAME}:${TAG}

# Se houver uma variável de ambiente chamada RADAR_TEST e seu valor for TRUE,
# então estamos num abiente de teste e, portanto, a imagem a ser criada e
# utilizada será a de teste.
ifeq (${RADAR_TEST}, TRUE)
	RADAR_IMAGE := ${RADAR_IMAGE_TEST}
else
	RADAR_IMAGE := ${RADAR_IMAGE_TAGGED}
endif

###############################################################################
#                           DOCKER RELATED VARIABLES                          #
###############################################################################
LOG_LINES = 100
FG = FALSE
NO_DEPS = FALSE

##################################################
# Comandos relacionados ao docker/docker-compose #
##################################################
DOCKER_COMPOSE := RADAR_IMAGE="${RADAR_IMAGE}" docker-compose
DOCKER_COMPOSE_TEST := RADAR_IMAGE="${RADAR_IMAGE_TEST}" RADAR_TEST="TRUE" docker-compose

DOCKER_UP := ${DOCKER_COMPOSE} up
DOCKER_LOGS := ${DOCKER_COMPOSE} logs --tail=${LOG_LINES}
DOCKER_START := ${DOCKER_COMPOSE} start
DOCKER_STOP := ${DOCKER_COMPOSE} stop

# Se não for solicitado explicitamente para rodarmos o projeto em foreground,
# então o projeto é executado no background como um daemon.
ifneq (${FG}, TRUE)
	DOCKER_UP += -d
endif

ifdef FOLLOW
	DOCKER_LOGS += -f
endif

# Utilizado para iniciar os serviços parcialmente, ignorando as "dependências"
# de cada um dos serviços solicitados para serem executados.
ifdef NO_DEPS
	DOCKER_UP += --no-deps
endif

# Aqui abre-se a possibilidade de se executar os comandos do docker-compose
# para serviços específicos. P.ex. Executar apenas o postgres e o django.
ifdef SERVICES
	DOCKER_UP += ${SERVICES}
	DOCKER_LOGS += ${SERVICES}
	DOCKER_STOP += ${SERVICES}
	DOCKER_START += ${SERVICES}
	DOCKER_RECREATE := ${DOCKER_COMPOSE} rm -s ${SERVICES}; ${DOCKER_UP}
endif

####################################
# Definindo os targets do Makefile #
####################################
.DEFAULT_GOAL : help

help:
	@echo "Welcome to Radar Parlamentar Makefile."
	@echo "This file is intended to ease your life regarding docker-compose commands."
	@echo "Bellow you will find the options you have with this makefile."
	@echo "You just need to run 'make <command> <arguments..>'."
	@echo "    "
	@echo "    help - Print this help message"
	@echo "    "
	@echo "    run [NO_DEPS=FALSE] [FG=FALSE] [SERVICES=ALL]"
	@echo "        Run the project in Background mode by default."
	@echo "        NO_DEPS: Define as TRUE to ignore service dependencies."
	@echo "        FG: Define as TRUE to run in foreground."
	@echo "        SERVICES: servies that will be affected by the command. E.g.: SERVICES='service_a_name service_b_name'"
	@echo "    "
	@echo "    logs [LOG_LINES=100] [SERVICES=ALL] [FOLLOW=TRUE]"
	@echo "        See services logs."
	@echo "        LOG_LINES: The number of log lines for each service"
	@echo "        SERVICES: servies that will be affected by the command. E.g.: SERVICES='service_a_name service_b_name'"
	@echo "        FOLLOW: keep tracking the logs. E.g.: FOLLOW=TRUE"
	@echo "    "
	@echo "    stop [SERVICES=ALL]"
	@echo "        Stop one or more services."
	@echo "        SERVICES: servies that will be affected by the command. E.g.: SERVICES='service_a_name service_b_name'"
	@echo "    "
	@echo "    start [SERVICES=ALL]"
	@echo "        Start one or more stopped services."
	@echo "        SERVICES: servies that will be affected by the command. E.g.: SERVICES='service_a_name service_b_name'"
	@echo "    "
	@echo "    recreate SERVICES='<service_a> <service_b> ...'"
	@echo "        Remove and recreate the given services/containers, but not their volumes."
	@echo "    "
	@echo "    build"
	@echo "        build the radar parlamentar docker image."
	@echo "    "
	@echo "    build-base"
	@echo "        build the base image for radar parlamentar docker image."
	@echo "    "
	@echo "    clean"
	@echo "        stop and remove all service containers and images."
	@echo "    "
	@echo "    clean-containers"
	@echo "        remove all containers, keeping images and volumes"
	@echo "    "
	@echo "    clean-volumes"
	@echo "        remove all volumes. It also remove containers"
	@echo "    "
	@echo "    clean-repo"
	@echo "        Clean the current repo by undoing all uncommited changes and remove untracked git files"
	@echo "    "
	@echo "    makemigrations"
	@echo "    "
	@echo "    migrate"
	@echo "    "
	@echo "    collectstatic"
	@echo "    "
	@echo "    update"
	@echo "        This command will execute 'makemigrations', 'migrate' and 'collectstatic' commands"
	@echo "    "
	@echo "    test"
	@echo "        This command will run Radar Parlamentar test suite"
	@echo "    "
	@echo "    dump-database"
	@echo "        Dump the database to the 'bkps/' directory preppended by the current date-time"
	@echo "    "
	@echo "    backup"
	@echo "        Backup both the database and the media files to 'bkps/' directory"
.PHONY: help

pull:
	@docker pull ${RADAR_IMAGE_LATEST} || true
	@docker pull ${RADAR_IMAGE} || true
.PHONY: run

run: pull
	@$(DOCKER_UP)
.PHONY: run

logs:
	$(DOCKER_LOGS)
.PHONY: logs

stop:
	$(DOCKER_STOP)
.PHONY: stop

start:
	@$(DOCKER_START)
.PHONY: start

recreate:
ifndef DOCKER_RECREATE
	@echo "You need to pass the services you want to recreate with SERVICE='service_a_name service_b_name'"
	@exit 1
else
	@$(DOCKER_RECREATE)
endif
.PHONY: recreate

build: pull
	${DOCKER_COMPOSE} build django
.PHONY: build

clean:
	${DOCKER_COMPOSE} down --rmi all
.PHONY: clean

clean-containers:
	${DOCKER_COMPOSE} down
.PHONY: clean-containers

clean-volumes:
	${DOCKER_COMPOSE} down --rmi -v
.PHONY: clean-containers

clean-repo:
	git reset --hard HEAD
	git clean -di -e '.env'
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
.PHONY: clean-pyc

makemigrations:
	${DOCKER_COMPOSE} up --no-deps -d postgres django
	${DOCKER_COMPOSE} exec django python manage.py makemigrations
	${DOCKER_COMPOSE} down
.PHONY: makemigrations

migrate:
	${DOCKER_COMPOSE} exec django python manage.py migrate
.PHONY: migrate

collectstatic:
	${DOCKER_COMPOSE} exec django python manage.py collectstatic --no-input
.PHONY: collectstatic

update: makemigrations migrate collecetstatic
.PHONY: update

test:
	${DOCKER_COMPOSE_TEST} up -d --no-deps django
	${DOCKER_COMPOSE_TEST} exec -T django python manage.py test
	${DOCKER_COMPOSE_TEST} down
.PHONY: test

dump-database:
	${DOCKER_COMPOSE} exec django sh -c "python manage.py dumpdata --natural-foreign -e sessions -e admin -e contenttypes -e auth.Permission > /radar/$(DB_BKP_FILE)"
	bzip2 -9 -f $(DB_BKP_FILE)
.PHONY: dump-database

backup: dump-database
	tar -cf $(MEDIA_BKP) radar_parlamentar/static/media
.PHONY: backup

release:
	docker pull ${RADAR_IMAGE_TEST}
	docker tag ${RADAR_IMAGE_TEST} ${RADAR_IMAGE_TAGGED}
	docker tag ${RADAR_IMAGE_TEST} ${RADAR_IMAGE_LATEST}
	docker push ${RADAR_IMAGE_TAGGED}
	docker push ${RADAR_IMAGE_LATEST}
