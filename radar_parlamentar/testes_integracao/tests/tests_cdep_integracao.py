# Copyright (C) 2012, 2013, Leonardo Leite, Diego Rabatone, Eduardo Hideo
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.


from django.test import TestCase
from importadores import cdep
from unittest import skip

# constantes relativas ao código florestal
ID = 17338
SIGLA = 'PL'
NUM = '1876'
ANO = '1999'
NOME = 'PL 1876/1999'


class CamarawsTest(TestCase):
    """Realiza testes envolvendo os web services da câmara"""

    def test_obter_proposicao(self):
        codigo_florestal_json = cdep.Camaraws.obter_proposicao_por_id(ID)

        self.assertEqual(codigo_florestal_json['siglaTipo'], SIGLA)
        self.assertEqual(str(codigo_florestal_json['numero']), NUM)
        self.assertEqual(str(codigo_florestal_json['ano']), ANO)

    def test_obter_votacoes(self):
        votacao_cod_florestal_json = cdep.Camaraws.obter_votacoes(ID)[1]
        uri_evento = votacao_cod_florestal_json['uriEvento']
        evento = cdep.Camaraws.obter_evento(uri_evento)
        data_vot_encontrada = evento['dataHoraInicio']
        self.assertEqual(data_vot_encontrada, '2011-05-24T20:01')

    def test_listar_proposicoes(self):
        pecs_2011_json = cdep.Camaraws.listar_proposicoes('PEC', '2011')
        self.assertEqual(len(pecs_2011_json), 135)

    def test_prop_nao_existe(self):
        id_que_nao_existe = -1
        with self.assertRaises(ValueError):
            cdep.Camaraws.obter_proposicao_por_id(id_que_nao_existe)

    def test_votacoes_nao_existe(self):
        with self.assertRaises(ValueError):
            cdep.Camaraws.obter_votacoes(-1)

    def test_listar_proposicoes_que_nao_existem(self):
        sigla = 'PEC'
        ano = '2113'
        self.assertEqual(cdep.Camaraws.listar_proposicoes(sigla, ano), [])

    def test_listar_siglas(self):
        siglas = cdep.Camaraws.listar_siglas()
        self.assertTrue('PL' in siglas)
        self.assertTrue('PEC' in siglas)
        self.assertTrue('MPV' in siglas)

    def test_votacao_presente_plenario(self):
        ANO = 2013
        NOME_PLENARIO = 'REQ 8196/2013'
        NOT_NOME_PLENARIO = 'DAVID 1309/1992'
        etree_plenario = cdep.Camaraws.obter_proposicoes_votadas_plenario(ANO)
        nome_prop_list = [
            nomeProp.find('nomeProposicao').text for nomeProp in etree_plenario
        ]
        self.assertTrue(NOME_PLENARIO in nome_prop_list)
        self.assertFalse(NOT_NOME_PLENARIO in nome_prop_list)
