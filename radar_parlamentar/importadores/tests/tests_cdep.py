# Copyright (C) 2012, 2013, Leonardo Leite, Diego Rabatone, Eduardo Hideo
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.


from django.test import TestCase
from importadores import cdep
from modelagem import models
import os
import json
import xml.etree.ElementTree as etree
import urllib.parse
from unittest.mock import Mock, patch
from importadores.tests.mocks_cdep import *
from unittest import skip

import logging

logging.disable()

ID_FLORESTAL = '17338'
NOME_FLORESTAL = 'PL 1876/1999'


class CamarawsTest(TestCase):

    def test_obter_proposicao_por_id(self):
        proposicao = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lambda _: mock_obter_proposicao(17338)):
            proposicao = cdep.Camaraws.obter_proposicao_por_id(17338)

        self.assertTrue(isinstance(proposicao, dict))
        self.assertEqual(proposicao['id'], 17338)

    def test_obter_votacoes(self):
        lista = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lambda _: mock_obter_votacoes('PL', 1876, 1999)):
            lista = cdep.Camaraws.obter_votacoes(17338)

        self.assertTrue(isinstance(lista, list))
        self.assertEqual(len(lista), 5)
        
    def test_obter_proposicoes_votadas_plenario(self):
        ANO = 2013
        NOME_PLENARIO = 'REQ 8196/2013'
        NOT_NOME_PLENARIO = 'DAVID 1309/1992'

        with patch.object(cdep.Camaraws,
                          'obter_proposicoes_votadas_plenario',
                          mock_obter_proposicoes_votadas_plenario):
            etree_plenario = cdep.Camaraws.obter_proposicoes_votadas_plenario(ANO)
            
        nome_prop_list = [
            nomeProp.find('nomeProposicao').text for nomeProp in etree_plenario
        ]
        self.assertTrue(NOME_PLENARIO in nome_prop_list)
        self.assertFalse(NOT_NOME_PLENARIO in nome_prop_list)

    def test_listar_proposicoes(self):
        page = 1
        def lam(_):
            nonlocal page
            m = mock_listar_proposicoes('PEC', 2011, page)
            page += 1
            return m

        lista = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lam):
            lista = cdep.Camaraws.listar_proposicoes('PEC', 2011)

        self.assertTrue(isinstance(lista, list))
        self.assertEqual(len(lista), 135)

    def test_obter_autores(self):
        lista = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lambda _: mock_obter_autores(17338)):
            lista = cdep.Camaraws.obter_autores(17338)

        self.assertTrue(isinstance(lista, list))
        self.assertEqual(len(lista), 1)
        self.assertEqual(lista[0]['nome'], 'Sérgio Carvalho')

    def test_obter_evento(self):
        dicionario = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lambda _: mock_obter_evento(25788)):

            dicionario = cdep.Camaraws.obter_evento(
            'https://dadosabertos.camara.leg.br/api/v2/eventos/25788')

        self.assertTrue(isinstance(dicionario, dict))
        self.assertEqual(dicionario['id'], 25788)

    def test_obter_votos(self):
        page = 1
        def lam(_):
            nonlocal page
            m = mock_obter_votos(4648, page)
            page += 1
            return m

        lista = None
        with patch.object(cdep.Camaraws,
                          '_read_and_parse',
                          lam):
            lista = cdep.Camaraws.obter_votos(4648)
        
        self.assertTrue(isinstance(lista, list))
        self.assertEqual(len(lista), 513)

    def test_listar_siglas(self):
        self.assertEqual(cdep.Camaraws.listar_siglas(),
                    ['PL', 'MPV', 'PDC', 'PEC', 'PLP',
                     'PLC', 'PLN', 'PLOA', 'PLS', 'PLV'])


class ProposicoesFinderTest(TestCase):

    def test_prop_in_xml(self):
        ano_min = ano_max = 2013

        with patch.object(cdep.Camaraws,
                          "obter_proposicoes_votadas_plenario",
                          mock_obter_proposicoes_votadas_plenario):
            dic_votadas = cdep.ProposicoesFinder.find_props_disponiveis(
                ano_max, ano_min)

        proposicao = {'id': '14245', 'sigla': 'PEC', 'num': '3', 'ano': '1999'}
        self.assertTrue(proposicao in dic_votadas)


class ImportadorCamaraTest(TestCase):

    @classmethod
    def setUpClass(cls):
        def votos(id_votacao):
            votos = []
            for page in range(1,7):
                votos += mock_obter_votos(id_votacao, page)['dados']
            return votos

        prop_por_id = lambda _: mock_obter_proposicao(17338)['dados']
        votacoes = lambda _: mock_obter_votacoes('PL',1876,1999)['dados']
        autores = lambda _: mock_obter_autores(17338)['dados']

        # vamos importar apenas o código florestal
        dic_votadas = [{'id': '17338', 'sigla': 'PL',
                        'num': '1876', 'ano': '1999'}]

        @patch.object(cdep.Camaraws, "obter_votos",             votos)
        @patch.object(cdep.Camaraws, "obter_proposicao_por_id", prop_por_id)
        @patch.object(cdep.Camaraws, "obter_votacoes",          votacoes) 
        @patch.object(cdep.Camaraws, "obter_autores",           autores)
        def f():
            importer = cdep.ImportadorCamara()
            importer.importar(dic_votadas)
            # chamando duas vezes pra testar idempotência
            importer.importar(dic_votadas)
        f()


    @classmethod
    def tearDownClass(cls):
        from util_test import flush_db
        flush_db(cls)

    def test_casa_legislativa(self):
        camara = models.CasaLegislativa.objects.get(nome_curto='cdep')
        self.assertEqual(camara.nome, 'Câmara dos Deputados')

    def test_prop_cod_florestal(self):
        data = cdep._converte_data('1999-10-19T00:00')
        prop_cod_flor = models.Proposicao.objects.get(id_prop=ID_FLORESTAL)
        self.assertEqual(prop_cod_flor.nome(), NOME_FLORESTAL)
        self.assertEqual(prop_cod_flor.situacao,
                         'Transformado em Norma Jurídica')
        self.assertEqual(prop_cod_flor.data_apresentacao.day, data.day)
        self.assertEqual(prop_cod_flor.data_apresentacao.month, data.month)
        self.assertEqual(prop_cod_flor.data_apresentacao.year, data.year)

    def test_votacoes_cod_florestal(self):
        votacoes = models.Votacao.objects.filter(
            proposicao__id_prop=ID_FLORESTAL)
        self.assertEqual(len(votacoes), 5)

        vot = votacoes[0]
        self.assertTrue('REQUERIMENTO DE RETIRADA DE PAUTA' in vot.descricao)

        data = cdep._converte_data('2011-05-24T20:01')
        vot = votacoes[1]
        self.assertEqual(vot.data.day, data.day)
        self.assertEqual(vot.data.month, data.month)
        self.assertEqual(vot.data.year, data.year)
    
    def test_votos_cod_florestal(self):
         votacao = models.Votacao.objects.filter(
             proposicao__id_prop=ID_FLORESTAL)[0]

         voto1 = [
             v for v in votacao.votos()
             if v.parlamentar.nome == 'Mara Gabrilli'][0]
         voto2 = [
             v for v in votacao.votos()
             if v.parlamentar.nome == 'Carlos Roberto'][0]
         self.assertEqual(voto1.opcao, models.SIM)
         self.assertEqual(voto2.opcao, models.NAO)
         self.assertEqual(voto1.parlamentar.partido.nome, 'PSDB')
         self.assertEqual(voto2.parlamentar.localidade, 'SP')

    def test_nao_tem_parlamentares_repetidos(self):
         todos = models.Parlamentar.objects.filter(
             casa_legislativa__nome_curto='cdep')
         self.assertTrue(todos.count() > 100)
         repetidos = []
         for p in todos:
             count_p = models.Parlamentar.objects.filter(
                 casa_legislativa__nome_curto='cdep', nome=p.nome,
                 partido__numero=p.partido.numero,
                 localidade=p.localidade).count()
             if count_p > 1:
                 repetidos.append(p)
         self.assertTrue(len(repetidos) == 0)    

