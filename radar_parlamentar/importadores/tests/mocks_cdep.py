# Copyright (C) 2012, Eduardo Hideo, Leonardo Leite
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.

import bz2
import os
import glob
import json
import xml.etree.ElementTree as etree
from importadores import cdep

MOCK_PATH = os.path.join(cdep.RESOURCES_FOLDER, 'mocks')


def parse_arquivo_xml(nome_arquivo):
    """retorna etree"""

    arquivo = os.path.join(MOCK_PATH, nome_arquivo)
    with bz2.open(arquivo, mode='rt') as arquivo_xml:
        return etree.fromstring(arquivo_xml.read())
    raise ValueError


def parse_arquivo_json(nome_arquivo):
    """retorna json como dicionario"""

    arquivo = os.path.join(MOCK_PATH, nome_arquivo)
    with bz2.open(arquivo, mode='rt') as arquivo_json:
        return json.load(arquivo_json)
            

def mock_obter_proposicao(id_prop):
    """retorna dicionario"""
    return parse_arquivo_json('proposicao_%s.json.bz2' % id_prop)

def mock_obter_votacoes(sigla, num, ano):
    """retorna dicionario"""
    return parse_arquivo_json('votacoes_%s%s%s.json.bz2' % (sigla, num, ano))

def mock_obter_proposicoes_votadas_plenario(ano):
    """retorna etree"""
    return parse_arquivo_xml('proposicoes_votadas_%s.xml.bz2' % ano)

def mock_listar_proposicoes(sigla, ano, pagina):
    """retorna dicionario"""
    return parse_arquivo_json('proposicoes_%s%s_%s.json.bz2' % (sigla, ano, pagina))

def mock_obter_autores(id_prop):
    """retorna dicionario"""
    return parse_arquivo_json('autores_proposicao_%s.json.bz2' % id_prop)

def mock_obter_evento(id_evento):
    """retorna dicionario"""
    return parse_arquivo_json('evento_%s.json.bz2' % id_evento)

def mock_obter_votos(id_votacao, pagina):
    """retorna dicionario"""
    return parse_arquivo_json('votos_%s_%s.json.bz2' % (id_votacao, pagina))

