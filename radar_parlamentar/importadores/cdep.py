# Copyright (C) 2012, Leonardo Leite, Diego Rabatone, Saulo Trento,
# Carolina Ramalho, Brenddon Gontijo Furtado
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.

"""módulo que cuida da importação dos dados da Câmara dos Deputados"""

from django.core.exceptions import ObjectDoesNotExist
from .chefes_executivos import ImportadorChefesExecutivos
from modelagem import models
from datetime import datetime
import urllib.request
import urllib.error
import urllib.parse
import logging
import json
import os

##### Legado
import xml.etree.ElementTree as etree


MODULE_DIR = os.path.abspath(os.path.dirname(__file__))
RESOURCES_FOLDER = os.path.join(MODULE_DIR, 'dados/cdep/')

ANO_MIN = 1991
# só serão buscadas votações a partir de ANO_MIN

logger = logging.getLogger("radar")

XML_FILE = 'dados/chefe_executivo/chefe_executivo_congresso.xml.bz2'
NOME_CURTO = 'cdep'


class Camaraws:

    """Acesso aos Web Services da Camara dos Deputados"""
    URL_PROPOSICAO = 'https://dadosabertos.camara.leg.br/api/v2/proposicoes?'
    URL_PROPOSICAO_POR_ID = 'https://dadosabertos.camara.leg.br/api/v2/proposicoes/'
    URL_VOTACOES_POR_ID = 'https://dadosabertos.camara.leg.br/api/v2/votacoes/'
    # API antiga pois a nova nao oferece a funcionalidade
    URL_PLENARIO = 'http://www.camara.gov.br/SitCamaraWS/' + \
                   'Proposicoes.asmx/ListarProposicoesVotadasEmPlenario?'

    @classmethod
    def _obter_paginas(cls, url, erro_str):
        dados = []
        normalize = lambda l: (l['rel'], l['href'])

        try:
            response = cls._read_and_parse(url)
            dados += response['dados']
            links = dict(map(normalize, response['links']))

            while links.get('next') is not None:
                response = cls._read_and_parse(links['next'])
                dados += response['dados']
                links = dict(map(normalize, response['links']))

        except urllib.error.HTTPError as error:
            logger.error(erro_str.format(error))
        return dados

    @staticmethod
    def _read_and_parse(url):
        text = b''
        try:
            request = urllib.request.Request(
                url, headers={'accept': 'application/json'})
            text = urllib.request.urlopen(request).read()

        except urllib.error.URLError as error:
            logger.error("%s ao acessar %s" % (error, url))

        return json.loads(text.decode())

    @classmethod
    def _obter_dados(cls, url, erro_str):
        try:
            return cls._read_and_parse(url)['dados']
        except urllib.error.HTTPError as error:
            logger.error(erro_str.format(error))
        return None

    @classmethod
    def obter_proposicao_por_id(cls, id_prop):
        """Obtém detalhes de uma proposição

        Argumentos:
        id_prop

        Retorna:
        Um dicionario correspondente ao JSON retornado pelo web service

        Exceções:
            urllib.error.HTTPError -- quando proposição não existe
        """
        return cls._obter_dados(cls.URL_PROPOSICAO_POR_ID + str(id_prop),
                            "{error} ao acessar proposicao " + str(id_prop))

    @classmethod
    def obter_votacoes(cls, id_prop):
        """
        Obtem votacoes de uma proposicao

        Argumentos:
        id_prop: id unico que caracteriza uma proposicao

        Retorna:
        Uma lista com as votacoes processadas retornadas pelo Web Service

        Excecoes:
        urllib.error.HTTPError -- quando proposicao nao existe
        """
        return cls._obter_dados(
            cls.URL_PROPOSICAO_POR_ID + str(id_prop) + '/votacoes',
            "{error} ao acessar proposicao " + str(id_prop))


    @staticmethod
    def obter_proposicoes_votadas_plenario(ano):
        """
        Obtem as votações votadas em Plenario

        Argumentos:
        obrigatorio : ano

        Retorna:
        Um objeto ElementTree correspondente ao XML retornado pelo web service
        Exemplo:
        http://www.camara.gov.br/sitcamaraws/Proposicoes.asmx/ListarProposicoesVotadasEmPlenario?ano=1991&tipo=
        """

        url = Camaraws.URL_PLENARIO + urllib.parse.urlencode(
            {'ano': ano, 'tipo': ' '})
        text = ''
        try:
            request = urllib.request.Request(url)
            text = urllib.request.urlopen(request).read()
        except (urllib.error.URLError, urllib.error.HTTPError) as erro:
            logger.error("{erro} ao acessar {url}".format(erro=erro, url=url))

        tree = None
        try:
            tree = etree.fromstring(text)
        except etree.ParseError as erro:
            logger.error("etree.ParseError: {erro}".format(erro=erro))


        if tree is None or tree.tag == 'erro':
            raise ValueError('O ano {} nao possui votacoes ainda'.format(ano))
        return tree

    @classmethod
    def listar_proposicoes(cls, sigla, ano):
        """Busca proposições de acordo com ano e sigla desejada

        Argumentos obrigatórios:
        sigla, ano -- strings que caracterizam as proposições buscadas

        Retorna:
        Uma lista correspondente a requisicao
        """

        return cls._obter_paginas(
            cls.URL_PROPOSICAO +
            urllib.parse.urlencode({
                'siglaTipo': sigla,
                'ano': ano,
                'ordem': 'ASC',
                'ordenarPor': 'id'}),
            "{error} ao acessar proposições de " + sigla + " de " + str(ano))

    @classmethod
    def obter_autores(cls, id_prop):
        """Busca autores de acordo com o id da proposição desejada

        Argumentos obrigatórios:
        id_prop -- identificador da proposição

        Retorna:
        Uma lista correspondente a requisicao
        """

        return cls._obter_dados(
            cls.URL_PROPOSICAO_POR_ID + str(id_prop) + '/autores',
            '{error} ao acessar autores da proposição ' + str(id_prop))

    @classmethod
    def obter_evento(cls, uri_evento):
        """
        Busca um evento de acordo com sua URI

        Argumentos obrigatórios:
        uri_evento -- URI do evento como indicado na votação

        Retorna:
        Um dicionario correspondente ao evento retornado pelo Web Service
        """

        evento = None

        if uri_evento is None:
            logger.info('URI de evento nula')
        else:
            evento = cls._obter_dados(
                uri_evento, '{error} ao acessar ' + uri_evento)

        return evento

    @classmethod
    def obter_votos(cls, id_votacao):
        """
        Busca votos da votação indicada

        Argumentos obrigatórios:
        id_votação -- o id da votação

        Retorna:
        Uma lista de votos
        """

        return cls._obter_paginas(
            cls.URL_VOTACOES_POR_ID +
            str(id_votacao) + '/votos',
            '{error} ao acessar votos da votação ' + str(id_votacao))

    @staticmethod
    def listar_siglas():
        """Listar as siglas de proposições existentes; exemplo: "PL", "PEC" etc.
        O retorno é feito em uma lista de strings.
        """
        # A lista completa se encontra aqui:
        # https://dadosabertos.camara.leg.br/api/v2/referencias/tiposProposicao
        # No entanto, muito dessas siglas correspondem a proposições que
        # não possuem votações
        # Por isso estamos aqui retornando um resultado mais restrito
        return ['PL', 'MPV', 'PDC', 'PEC', 'PLP',
                'PLC', 'PLN', 'PLOA', 'PLS', 'PLV']

    @staticmethod
    def listar_siglas_string():
        """Listar as siglas de proposições existentes; exemplo: "PL", "PEC" etc.
        O retorno é feito em uma string com as siglas separadas por virgulas.
        """
        # A lista completa se encontra aqui:
        # https://dadosabertos.camara.leg.br/api/v2/referencias/tiposProposicao
        # No entanto, muito dessas siglas correspondem a proposições que
        # não possuem votações
        # Por isso estamos aqui retornando um resultado mais restrito
        return 'PL,MPV,PDC,PEC,PLP,PLC,PLN,PLOA,PLS,PLV'

class ProposicoesFinder:
    """
    Classe legada, a nova API nao oferece a mesma funcionalidade.
    """

    @classmethod
    def find_props_disponiveis(cls, ano_max=None, ano_min=ANO_MIN):
        """Retorna uma lista com proposicoes que tiveram votações
        entre ano_min e ano_max.
        Cada votação é um dicionário com chaves \in {id, sigla, num, ano}.
        As chaves e valores desses dicionários são strings.

        ano_min padrão é 1991
        """

        if ano_max is None:
            ano_max = datetime.today().year

        proposicoes_votadas = []
        for ano in range(ano_min, ano_max + 1):
            logger.info('Procurando em %s' % ano)
            try:
                proposicoes = Camaraws.obter_proposicoes_votadas_plenario(ano)
                proposicoes = cls._parse_xml(proposicoes)
                proposicoes_votadas.extend(proposicoes)
                logger.info('%d proposições encontradas' %
                            len(proposicoes))
            except Exception as e:
                logger.error(e)
        return proposicoes_votadas[::-1]


    @classmethod
    def _parse_xml(cls, xml):
        prop_votadas = []
        for child in xml:
            id_prop = child.find('codProposicao').text.strip()
            nome_prop = child.find('nomeProposicao').text.strip()
            dic_prop = cls._build_dic(id_prop, nome_prop)
            prop_votadas.append(dic_prop)
        return prop_votadas

    @staticmethod
    def _build_dic(id_prop, nome_prop):
        sigla = nome_prop[0:nome_prop.index(" ")]
        num = nome_prop[nome_prop.index(" ") + 1: nome_prop.index("/")]
        ano = nome_prop[nome_prop.index("/") + 1: len(nome_prop)]
        return {'id': id_prop, 'sigla': sigla, 'num': num, 'ano': ano}


def _converte_data(data_str):
    """Converte string 'a-m-dTh:min' para objeto datetime.date;
    retona None se data_str é inválido
    """ 
    try:
        return  datetime.strptime(data_str, '%Y-%m-%dT%M:%S')
    except ValueError:
        return None

class ImportadorCamara:
    """Salva os dados dos web services da
    Câmara dos Deputados no banco de dados"""

    def __init__(self):
        self.camara_dos_deputados = self._gera_casa_legislativa()
        self.parlamentares = self._init_parlamentares()
        self.proposicoes = self._init_proposicoes()
        self.votacoes = self._init_votacoes()
        self.opcoes_voto_desconhecidas = []
        self.partidos_desconhecidos = []

    def _gera_casa_legislativa(self):
        """Gera objeto do tipo CasaLegislativa
        Câmara dos Deputados e o salva no banco de dados.
        Caso cdep já exista no banco de dados, retorna o objeto já existente.
        """
        count_cdep = models.CasaLegislativa.objects.filter(
            nome_curto='cdep').count()
        if not count_cdep:
            camara_dos_deputados = models.CasaLegislativa()
            camara_dos_deputados.nome = 'Câmara dos Deputados'
            camara_dos_deputados.nome_curto = 'cdep'
            camara_dos_deputados.esfera = models.FEDERAL
            camara_dos_deputados.save()
            return camara_dos_deputados
        else:
            return models.CasaLegislativa.objects.get(nome_curto='cdep')

    def _init_parlamentares(self):
        """(nome_parlamentar,nome_partido,localidade) -> Parlamentar"""
        parlamentares = {}
        for p in models.Parlamentar.objects.filter(
                casa_legislativa=self.camara_dos_deputados):
            parlamentares[self._key_parlamentar(p)] = p
        return parlamentares

    def _key_parlamentar(self, parlamentar):
        return (parlamentar.nome,
                parlamentar.partido.nome,
                parlamentar.localidade)

    def _init_proposicoes(self):
        """id_prop -> Proposicao"""
        proposicoes = {}
        for p in models.Proposicao.objects.filter(
                casa_legislativa=self.camara_dos_deputados):
            proposicoes[p.id_prop] = p
        return proposicoes

    def _init_votacoes(self):
        """(id_prop,descricao,data) -> Votacao"""
        votacoes = {}
        for v in models.Votacao.objects.filter(
                proposicao__casa_legislativa=self.camara_dos_deputados):
            votacoes[self._key_votacao(v)] = v
        return votacoes

    def _key_votacao(self, votacao):
        return (votacao.proposicao.id_prop, votacao.descricao, votacao.data)

    def importar(self, votadas):
        """votadas -- lista de dicionários com
            id/sigla/num/ano das proposições que tiveram votações
        """
        self.total_proposicoes = len(votadas)
        self.proposicoes_importadas = 0
        self.imprimir_quando_progresso = 5
        for proposicao in votadas:
            self._importar(proposicao)
            self._progresso()

    def _progresso(self):
        self.proposicoes_importadas += 1
        fracao = self.proposicoes_importadas / self.total_proposicoes
        porcentagem = 100.0 * fracao
        if porcentagem > self.imprimir_quando_progresso:
            logger.info('Progresso: %.1f%%' % porcentagem)
            self.imprimir_quando_progresso += 5

    def _importar(self, dic_proposicao):
        """dic_proposicao -- dicionário com
            id/sigla/num/ano de uma proposição a ser importada
        """
        id_prop = dic_proposicao['id']
        sigla = dic_proposicao['sigla']
        num = dic_proposicao['num']
        ano = dic_proposicao['ano']

        try:
            if id_prop in self.proposicoes:
                prop = self.proposicoes[id_prop]
            else:
                prop_from_json = Camaraws.obter_proposicao_por_id(id_prop)
                prop = self._prop_from_json(prop_from_json)

            for votacao in Camaraws.obter_votacoes(id_prop):
                self._votacao_from_json(votacao, prop)
        except ValueError as error:
            logger.error("ValueError: %s" % error)

    def _prop_from_json(self, prop_json):
        """prop_json -- tipo dicionario

        Retorna proposicao
        """
        id_prop = str(prop_json['id'])
        prop = models.Proposicao()
        prop.id_prop = id_prop
        prop.sigla = prop_json['siglaTipo']
        prop.numero = str(prop_json['numero'])
        prop.ano = str(prop_json['ano'])
        logger.info("Importando %s %s/%s" % (
            prop.sigla, prop.numero, prop.ano))
        prop.ementa = prop_json['ementa']
        prop.descricao = prop_json['ementaDetalhada']
        prop.indexacao = prop_json['keywords']
        prop.autor_principal = Camaraws.obter_autores(
            prop_json['id'])[0]['nome']
        prop.data_apresentacao = _converte_data(prop_json['dataApresentacao'])
        prop.situacao = prop_json['statusProposicao']['descricaoSituacao']
        prop.casa_legislativa = self.camara_dos_deputados
        prop.save()
        self.proposicoes[id_prop] = prop
        return prop

    def _votacao_from_json(self, votacao_json, prop):
        """votacao_json -- dicionario representando votação (Json)
           prop -- objeto do tipo models.Proposicao
        """
        descricao = votacao_json['titulo']
        uri_evento = votacao_json['uriEvento']
        data = uri_evento and _converte_data(
            Camaraws.obter_evento(uri_evento)['dataHoraInicio'])
        key = (prop.id_prop, descricao, data)
        if key not in self.votacoes:
            votacao = models.Votacao()
            votacao.proposicao = prop
            votacao.descricao = descricao
            votacao.data = data
            votacao.save()
            self.votacoes[key] = votacao
            for voto_json in Camaraws.obter_votos(votacao_json['id']):
                self._voto_from_json(voto_json, votacao)

    def _voto_from_json(self, voto_json, votacao):
        """voto_json -- dicionario representando voto em JSON
           retornado pelo Web service
           votacao -- objeto do tipo models.Votacao
        """
        opcao_str = voto_json['voto']
        deputado = self._deputado(voto_json)
        voto = models.Voto()
        voto.opcao = self._opcao_xml_to_model(opcao_str)
        voto.parlamentar = deputado
        voto.votacao = votacao
        voto.save()

    def _opcao_xml_to_model(self, voto):
        if voto == 'Não':
            return models.NAO
        elif voto == 'Sim':
            return models.SIM
        elif voto == 'Obstrução':
            return models.OBSTRUCAO
        elif voto == 'Abstenção':
            return models.ABSTENCAO
        elif voto == 'Art. 17':
            return models.ABSTENCAO
        else:
            if not voto in self.opcoes_voto_desconhecidas:
                self.opcoes_voto_desconhecidas.append(voto)
                logger.warning(
                    'opção de voto "%s" desconhecido! Mapeado como ABSTENCAO'
                    % voto)
            return models.ABSTENCAO

    def _deputado(self, voto_json):
        """Procura primeiro no cache e depois no banco; se não existir,
        cria novo parlamentar"""
        nome = voto_json['parlamentar']['nome'].title()
        nome_partido = voto_json['parlamentar']['siglaPartido']
        partido = self._partido(nome_partido)
        localidade = voto_json['parlamentar']['siglaUf']
        key = (nome, partido.nome, localidade)
        parlamentar = self.parlamentares.get(key)
        if not parlamentar:
            parlamentar = models.Parlamentar()
            parlamentar.parlamentar_id = voto_json['parlamentar']['id']
            parlamentar.nome = nome
            parlamentar.partido = partido
            parlamentar.localidade = localidade
            parlamentar.casa_legislativa = self.camara_dos_deputados
            parlamentar.save()
            if partido.numero == 0:
                if not nome_partido in self.partidos_desconhecidos:
                    self.partidos_desconhecidos.append(nome_partido)
                    logger.warn('Não achou o partido %s. Mapeado como "sem partido"'
                        % nome_partido)
            self.parlamentares[key] = parlamentar
        return parlamentar

    def _partido(self, nome_partido):
        nome_partido = nome_partido
        partido = models.Partido.from_nome(nome_partido)
        if partido is None:
            partido = models.Partido.get_sem_partido()
        return partido


class PosImportacao:

    def processar(self):
        self.remover_votacao_com_deputados_sem_partidos()

    # Issue #256
    def remover_votacao_com_deputados_sem_partidos(self):
        try:
            prop = models.Proposicao.objects.get(sigla='PL',
                                                 numero='821',
                                                 ano='1995')
            obj_votacao = 'SUBEMENDA A EMENDA N. 33'
            votacao = models.Votacao.objects.get(
                proposicao=prop, descricao__contains=obj_votacao)
            votacao.delete()
        except ObjectDoesNotExist:
            logger.warn('Votação esperada (em PL 821/1995)\
                        não foi encontrada na base de dados.')

def main():
    logger.info('IMPORTANDO DADOS DA CAMARA DOS DEPUTADOS')
    dic_votadas = ProposicoesFinder.find_props_disponiveis()
    importador = ImportadorCamara()
    importador.importar(dic_votadas)
    pos_importacao = PosImportacao()
    pos_importacao.processar()
    logger.info('IMPORTANDO CHEFES EXECUTIVOS DA CAMARA DOS DEPUTADOS')
    importer_chefe = ImportadorChefesExecutivos(
        NOME_CURTO, 'Presidentes', 'Presidente', XML_FILE)
    importer_chefe.importar_chefes()

    from importadores import cdep_genero
    cdep_genero.main()
    logger.info('IMPORTACAO DE DADOS DA CAMARA DOS DEPUTADOS FINALIZADA')
