import subprocess
from radar_parlamentar.settings import TIME_ZONE
import logging
from django import template
from django.utils.html import format_html
register = template.Library()


logger = logging.getLogger("radar")


@register.simple_tag
def versao_radar():
    time_zone = TIME_ZONE

    try:
        cmd_data_ultimo_commit = 'TZ={0} date -d @$(git log \-n1 --format='
        cmd_data_ultimo_commit += '"%at") "+%d/%m/%Y"'.format(time_zone)
        data_ultimo_commit = subprocess.check_output(cmd_data_ultimo_commit,
                                                     shell=True)

        cmd_hash_ultimo_commit = 'git log --pretty=format:"%H" -n 1'
        hash_ultimo_commit = subprocess.check_output(cmd_hash_ultimo_commit,
                                                     shell=True)
        hash_abrev_ultimo_commit = hash_ultimo_commit[:7]

        content = (f"Versão: <a href='https://gitlab.com/radar-parlamentar/"
                   f"radar/commit/{hash_ultimo_commit.decode('utf-8')}' "
                   f"target='_blank'>"
                   f"{hash_abrev_ultimo_commit.decode('utf-8')}</a> de "
                   f"{data_ultimo_commit.decode('utf-8')}")
        return format_html(content)

    except (IndexError, subprocess.CalledProcessError) as e:
        logger.error('Erro ao pegar o hash ou' +
                     'a data do ultimo commit (versao sera omitida)')

    return ""
