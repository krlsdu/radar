radar
  |
  |-deploy	// Backend
  |  |-elasticsearch	// Redutor de palavras
  |  >entrypoint.sh	// Entrada do Docker
  |  >radar_uwsgi.sh	// uWSGI Web server
  |  >radar.nginx.conf	// Nginx proxy reverso
  |  >uswgi_params	// uWSGI Web server
  |
  |-doc		// Algumas documentações
  |
  |-radar_parlamentar
  |  |
  |  |-analises	// Hackaton com análises
  |  |  >analise.py	// Inicializa as análises
  |  |  >filtro.py	// Acessa DB e retorna dados
  |  |  >genero.py	// Acessa DB e retorna dados de gênero e "tag cloud"
  |  |  >grafico.py	// Visual dos gráficos
  |  |  >models.py
  |  |  >pca.py		// Análise matemática
  |  |  >views.py	// Chama as análises e salva os JSON em radar/json
  |  |
  |  |-cron	// Cron jobs
  |  |
  |  |-exportadores
  |  |  >exportador_csv_r.py	// Gera CSV para R
  |  |  >exportador_csv.py	// Gera CSV
  |  |  >models.py		// vazio
  |  |  >views.py		// vazio
  |  |
  |  |-htmlcov
  |  |
  |  |-importadores
  |  |
  |  |-modelagem
  |  |
  |  |-plenaria
  |  |
  |  |-radar_parlamentar
  |  |
  |  |-sonar
  |  |
  |  |-static
  |  |
  |  |-testes_integracao
  |  |
  |  |-util_test
  |
  |-sockets
  |  >radar.sock // Comunicação dos dockers


